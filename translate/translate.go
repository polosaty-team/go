package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	// "unicode/utf8"
	// "strings"
)

// // type maptype map[string]string
type maptype map[rune]rune

func _tr(src []rune, trmap maptype) (dst string, changed int) {
	dst = ""
	changed = 0

	for _, i := range src {
		char, ok := trmap[i]
		if !ok {
			dst = dst + string(i)
		} else {
			dst = dst + string(char)
			changed += 1
		}
	}
	// return dst, changed
	return
}

func main() {

	s1 := []rune("qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?`~!@#$%^&*()_+|")
	s2 := []rune("йцукенгшщзхъфывапролджэячсмитьбю.ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,ёЁ!\"№;%:?*()_+/")
	// info, err := os.Stdin.Stat()
	// if err != nil {
	//     panic(err)
	// }

	// DOESN'T WORKS
	// if info.Mode()&os.ModeCharDevice != 0 || info.Size() <= 0 {
	// 	fmt.Println("The command is intended to work with pipes.")
	// 	fmt.Println("Usage: fortune | gocowsay")
	// 	return
	// }

	s1s2 := make(maptype)
	s2s1 := make(maptype)

	for pos, char := range s2 {

		s2s1[char] = s1[pos]
	}

	for pos, char := range s1 {
		s1s2[char] = s2[pos]
	}

	reader := bufio.NewReader(os.Stdin)
	var output []rune

	for {
		input, _, err := reader.ReadRune()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)

	}
	output_translated_s1s2, diff1 := _tr((output), s1s2)
	output_translated_s2s1, diff2 := _tr((output), s2s1)

	if diff1 > diff2 {
		fmt.Printf(output_translated_s1s2)
	} else {
		fmt.Printf(output_translated_s2s1)
	}

}
