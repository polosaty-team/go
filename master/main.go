package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os/exec"
	"strings"
)

func main() {

	cmd := exec.Command("..\\answer\\answer.exe", "login")
	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}
	stdoutRdr := bufio.NewReader(stdout)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}

	// cmd.Stdin
	// buffer := bytes.Buffer{}
	// buffer.Write([]byte("username\npassword\n"))
	// outbuff := bytes.Buffer{}
	// errbuff := bytes.Buffer{}
	// cmd.Stdin = &buffer
	// cmd.Stdout = &outbuff
	// cmd.Stderr = &errbuff

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	promptString, err := stdoutRdr.ReadString(':')
	if err != nil {
		log.Fatal(err)
	}

	// io.WriteString(stdin, "1\n")

	go func() {
		slurpErr, _ := ioutil.ReadAll(stderr)
		fmt.Printf("stderr: %s\n", slurpErr)
	}()

	// slurp, _ := ioutil.ReadAll(stdout)
	// slurp, err := stdout.Read(p []byte)
	fmt.Printf("promptString: %s\n", promptString)
	// outstr := string(slurp)
	if strings.Contains(promptString, "Enter number x:") {

		// defer func() {
		// 	err := stdin.Close()
		// 	if err != nil {
		// 		log.Println("Error on stdin.Close():", err)
		// 	}
		// }()
		io.WriteString(stdin, "10\n")

	}

	promptString, err = stdoutRdr.ReadString(':')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("promptString: %s\n", promptString)

	if strings.Contains(promptString, "Enter number y:") {
		_, err = io.WriteString(stdin, "20\n")
		if err != nil {
			log.Println("Writing error:", err)
		}

	}
	slurp, _ := ioutil.ReadAll(stdout)
	fmt.Printf("stdout: %s\n", slurp)
	slurperr, _ := ioutil.ReadAll(stdout)
	fmt.Printf("stderr: %s\n", slurperr)
}
