package main

import (
	// "bufio"
	"fmt"
	"log"
	// "os"
	// "strconv"
)

func promptNumber(prompt string) (int, error) {
	fmt.Print(prompt)
	var x int
	_, err := fmt.Scanf("%d\n", &x)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	return x, nil
}

func promptNumberAndHndleErr(prompt string) int {
	x, err := promptNumber(prompt)
	if err != nil {
		log.Fatal(err)
	}
	return x
}

func main() {
	// reader := bufio.NewReader(os.Stdin)
	x := promptNumberAndHndleErr("Enter number x: ")
	y := promptNumberAndHndleErr("Enter number y: ")

	fmt.Println("x * y =", x*y)
}
